package example;

import java.util.logging.*;

public class Example01 {
    // Obtain a suitable logger.
    private static Logger logger = Logger.getLogger("example");

    public static void main(String argv[]) {
        // Log a FINE tracing message
        logger.fine("doing stuff");
        try {
            throw new Exception("dummy exception");
        } catch (Exception ex) {
            // Log the exception
            logger.log(Level.WARNING, "trouble sneezing", ex);
        }
        logger.fine("done");
    }
}
