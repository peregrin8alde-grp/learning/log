= Java 言語におけるログ出力方法


== 標準機能でのログ出力

* `java.util.logging` パッケージを利用
** 参考 : link:https://docs.oracle.com/javase/jp/8/docs/technotes/guides/logging/overview.html[Javaロギングの概要]

.Example01
[#ex-example01]
====
. 以下のソースを用意
+
[source,java]
----
include::source/example/Example01.java[]
----
. 以下でビルド
+
[source,shell]
----
javac -d ./_build source/example/Example01.java
----
. 以下で実行
+
[source,shell]
----
java -cp ./_build example.Example01 
----
====

.View result of <<ex-example01>>
[example%collapsible.result]
====
[source,bash]
----
12 19, 2021 6:48:41 午後 example.Example01 main
警告: trouble sneezing
java.lang.Exception: dummy exception
        at example.Example01.main(Example01.java:13)

----
====

== ライブラリを利用したログ出力

* ログ出力ライブラリ
** https://logback.qos.ch/[Logback] : SLF4J から使われること前提のログ出力実装
** https://logging.apache.org/log4j/2.x/[log4j2] : log4j のバージョン 2
* ログファサードライブラリ
** https://www.slf4j.org/[SLF4J] : 各ログ出力実装を統合する I/F

=== SLF4J + Logback

.Example02
[#ex-example02]
====
. プロジェクトの初期化
+
[source,java]
----
docker run \
  -it --rm \
  -u 1000:1000 \
  -v `pwd`:/prj \
  -w /prj \
  -e MAVEN_CONFIG=/prj/maven/.m2 \
  maven:3.9 \
    mvn archetype:generate \
      -Duser.home=/prj/maven \
      -DinteractiveMode=false \
      -DarchetypeGroupId=org.apache.maven.archetypes \
      -DarchetypeArtifactId=maven-archetype-simple \
      -DarchetypeVersion=1.4 \
      -DgroupId=sample \
      -DartifactId=logback01 \
      -Dversion=1.0-SNAPSHOT \
      -Dpackage=sample.logback01
----
. 以下のソースを用意
+
.App.java
[%collapsible]
======
[source,java]
----
include::logback01/src/main/java/sample/logback01/App.java[]
----
======
+
.logback.xml
[%collapsible]
======
[source,xml]
----
include::logback01/src/main/config/logback.xml[]
----
======
. 以下の pom.xml を用意
+
.pom.xml
[%collapsible]
======
[source,java]
----
include::logback01/pom.xml[]
----
======
. 以下でビルド
+
[source,shell]
----
docker run \
  -it --rm \
  -u 1000:1000 \
  -v `pwd`/maven:/prj/maven \
  -v "`pwd`/logback01":"/prj/logback01" \
  -w /prj/logback01 \
  -e MAVEN_CONFIG=/prj/maven/.m2 \
  maven:3.9 \
    mvn clean package -Duser.home=/prj/maven
----
. 以下で実行
+
[source,shell]
----
docker run \
  -it --rm \
  -u 1000:1000 \
  -v `pwd`/maven:/prj/maven \
  -v "`pwd`/logback01":"/prj/logback01" \
  -w /prj/logback01 \
  -e MAVEN_CONFIG=/prj/maven/.m2 \
  maven:3.9 \
    mvn exec:java -Duser.home=/prj/maven -Dexec.mainClass=sample.logback01.App
----
====

.View result of <<ex-example02>>
[%collapsible]
====
.console
[source,bash]
----
09:01:09.465 [sample.logback01.App.main()] INFO  sample.logback01.App -- info msg
09:01:09.468 [sample.logback01.App.main()] ERROR sample.logback01.App -- error msg
09:01:09.469 [sample.logback01.App.main()] DEBUG sample.logback01.App -- debug msg
Hello World!
----
.info.log
[source,txt]
----
include::logback01/log/info.log[]
----
.error.log
[source,txt]
----
include::logback01/log/error.log[]
----
.debug.log
[source,txt]
----
include::logback01/log/debug.log[]
----
====

