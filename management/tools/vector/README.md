# Vector

- https://vector.dev/
- https://vector.dev/docs/


## install

OS のパッケージマネージャーや Vector のインストーラー利用推奨。

```
# download
mkdir -p vector && \
  curl -sSfL --proto '=https' --tlsv1.2 https://packages.timber.io/vector/0.16.1/vector-0.16.1-x86_64-unknown-linux-musl.tar.gz  | \
  tar xzf - -C vector --strip-components=2

# set PATH
## .bash_profile を使わない場合（ .bash_profile が存在すると .profile が読み込まれない）
echo "export PATH=\"$(pwd)/vector/bin:\$PATH\"" >> $HOME/.profile
source $HOME/.profile

## .bash_profile を使う場合
echo "export PATH=\"$(pwd)/vector/bin:\$PATH\"" >> $HOME/.bash_profile
source $HOME/.bash_profile
```

## quick start

```
# dummy generator
vector --config vector/config/vector.toml

# stdio
vector --config vector/config/examples/stdio.toml
```