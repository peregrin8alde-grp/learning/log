# Logstash

- https://www.elastic.co/jp/logstash/
- https://www.elastic.co/guide/en/logstash/current/index.html


## install

https://www.elastic.co/guide/en/logstash/current/installing-logstash.html

`Apache 2.0` ライセンス版を利用

```
curl -LO https://artifacts.elastic.co/downloads/logstash/logstash-oss-7.14.1-linux-x86_64.tar.gz

tar zxvf ./logstash-oss-*-linux-x86_64.tar.gz
```

## quick start

```
cd logstash
bin/logstash -e 'input { stdin { } } output { stdout {} }'
```

- 起動メッセージが出た後に標準入力に文字を打ち込むと、メッセージとして標準出力に出力される。
- `CTRL-D` で停止
